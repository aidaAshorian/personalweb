import React from 'react';
import girlIcon from '../svg/girl.svg';
import styles from '../styles/Footer.module.css';

function Footer(props) {
    return (
        <footer className={styles.footer}>
             <p>designed by </p> <span> Aida </span><img className={styles.icon} src={girlIcon} alt='icon'/> 
        </footer>
    );
}

export default Footer;