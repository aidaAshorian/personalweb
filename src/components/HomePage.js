import React from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { Avatar } from '@mui/material';
import styles from '../styles/HomePage.module.css';
import banner from '../svg/banner.png';
import Header from './Header';
import gitlab from '../svg/gitlab.svg';
import { GET_RESUME } from '../graphql/querys';
import Loader from '../shared/Loader';

function HomePage() {
    const {data,loading} = useQuery(GET_RESUME);
    const urlReasume = data?.reasumes[0].myReasume.url;
    console.log(urlReasume)
    return (
        <div className={styles.container}>
                <Header/>
        {loading ? <Loader/> :
            <div className={styles.main}>
                <img  className={styles.banner} src={banner} alt='banner'/>
                    <div className={styles.text}>
                        <h1>I am Aida Ashorian</h1>
                        <p>I help designers, small agencies and businesses bring their ideas to life.</p>
                        <button><Link to='/projects'>Get Started</Link></button>
                        <a className={styles.reasume} href={`${urlReasume}`} target='blank'>Reasume</a>
                        <a href='https://gitlab.com/aidaAshorian'>
                         <Avatar src={gitlab} />
                        </a>
                    </div>
            </div>
        }
        </div>
    );
}

export default HomePage;