import React from 'react';
import {Link} from 'react-router-dom';
import styles from '../styles/Header.module.css';

function Header() {
    return (
        <div className={styles.container}>
            <Link to='/home'><p>Home</p></Link>
           <Link to='/projects'> <p>Projects</p></Link>
        </div>
    );
}

export default Header;