import React from 'react';
import { useQuery } from '@apollo/client';
import Grid from '@mui/material/Grid';
import styles from '../styles/MyProjects.module.css';
import Header from './Header';
import { GET_BLOGS_INFO } from '../graphql/querys';
import CardEl from '../shared/CardEl';
import Loader from '../shared/Loader';

function MyProjects() {
    const {loading, data} = useQuery(GET_BLOGS_INFO);
    return (
        <div className={styles.container}>
            <Header/>
            <Grid container spacing={2}>
                {loading ? <Loader/> :
                data.posts.map((post) =>(
                    <Grid item xs={12} sm={6} md={4} key={post.id}>
                        <CardEl {...post}/>
                  </Grid>
                ))}
            </Grid>
        </div>
    );
}

export default MyProjects;