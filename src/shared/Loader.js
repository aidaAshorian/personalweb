import React from 'react';
import {InfinitySpin} from 'react-loader-spinner';

function Loader() {
    return (
        <div style={{width:'100%',height:'1000px',textAlign:'center'}}>
            <InfinitySpin width='200'  visible={true} color="#4fa94d" ariaLabel="infinity-spin-loading"/>
        </div>
    );
}

export default Loader;