import React from 'react';
import { Card,CardContent,CardHeader,CardMedia,CardActions,Button,Divider,Avatar,Typography } from '@mui/material';
import gitlab from '../svg/gitlab.svg';
import icon from '../svg/react.svg';

function CardEl({title,slug,coverphoto,content,vercelUrl,gitUrl}) {
    return (
        <Card sx={{boxShadow:'rgba(0,0,0,0.1) 0px 4px 12px' , borderRadius:4,margin:'10px 20px'}}>
            <CardHeader style={{backgroundColor:'#b8daca'}} avatar={<Avatar src={icon} sx={{marginRight:2}}/>}
                  title={
                  <Typography component='p' variant='p' color='text.secondary'>
                    {title}
                  </Typography>
                  }
            />
           <CardMedia component="img" 
            height="194"
             image={coverphoto[0].url}
             alt={slug}
            />
            <CardContent>
                <Typography  component='p' variant='p' color='text.primary' fontWeight={200}>
                    {content.text}
                </Typography>
            </CardContent>
            <Divider variant='middle' sx={{margin:'10px'}}/>
            <CardActions style={{display:'flex',justifyContent:'space-between'}}>
                  <Button size='small' sx={{width:'fit-content' , borderRadius:3}}>
                      <a href={`${gitUrl}`} style={{display:'flex',alignItems:'center',textDecoration:'none',color:'#000000'}}>
                      <Avatar src={gitlab} />
                      </a>
                   </Button>
                   <Button style={{backgroundColor:'#23875f'}} variant='outlined' size='small' sx={{width:'fit-content' , borderRadius:3}}>
                      <a href={`${vercelUrl}`} style={{display:'flex',alignItems:'center',textDecoration:'none',color:'#000000'}}>
                        visit
                      </a>
                   </Button>
            </CardActions>
        </Card>
    );
}

export default CardEl;