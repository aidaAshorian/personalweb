import {gql } from '@apollo/client';
const GET_BLOGS_INFO =gql`
query Posts {
    posts {
    id
    slug
    title
    vercelUrl
    gitUrl
    content {
      text
    }
    coverphoto {
      url
    }
  }
}
`;
const GET_RESUME = gql`
query reasumes {
  reasumes{
    myReasume{
      url
    }
  }
}
`;
export {GET_BLOGS_INFO,GET_RESUME};