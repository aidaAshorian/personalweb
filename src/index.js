import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter} from 'react-router-dom';
import { ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';
import './index.css';
import App from './App';

const client = new ApolloClient({
  uri:process.env.REACT_APP_GRAPHCMS_URL,
  cache: new InMemoryCache(),
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <ApolloProvider client={client}>
      <BrowserRouter>
         <App />
      </BrowserRouter>
   </ApolloProvider>
);

