import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import HomePage from './components/HomePage';
import Footer from './components/Footer';
import MyProjects from './components/MyProjects';

function App(props) {
  return (
   <>
      <Routes>
         <Route path='/home' element={<HomePage/>}/>
         <Route path='/projects' element={<MyProjects/>}/>
         <Route path='/*' element={<Navigate to='/home'/>}/>
      </Routes>
      <Footer/>
    </>
  );
}

export default App;